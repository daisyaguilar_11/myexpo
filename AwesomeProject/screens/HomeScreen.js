import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Button
              title="Go to Details"
              onPress={() => this.props.navigation.navigate('Details')}
            />
            <Button
              title="Go to Activity Indicator Component"
              onPress={() => this.props.navigation.navigate('ActivityIndicator')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  

HomeScreen.propTypes = {

};

export default HomeScreen;
